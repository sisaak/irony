﻿using System;
using System.Collections.Generic;
using Avala.Irony;
using Irony.Interpreter;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Irony.Tests
{
    [TestClass]
    public class SimpleStringEvaluateTests
    {
        [TestMethod]
        public void TestEvaluateLiteral()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            var result = scriptApp.Evaluate("\"hi\";");
            Assert.AreEqual("hi", result);
        }

        [TestMethod]
        public void TestEvaluateVariable()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            scriptApp.Globals["parameters"] = new Dictionary<string, string> { { "OwnerLastName", "Isaak" } };
            var result = scriptApp.Evaluate("${OwnerLastName};");
            Assert.AreEqual("Isaak", result);
        }

        [TestMethod]
        public void TestEvaluateConcatCall()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            var result = scriptApp.Evaluate("concat(\"hi\", \" ok\");");
            Assert.AreEqual("hi ok", result);
        }

        [TestMethod]
        public void TestEvaluateReplaceCall()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            var result = scriptApp.Evaluate("replace(\"hi you!\", \"you\", \"Scott\");");
            Assert.AreEqual("hi Scott!", result);
        }

        [TestMethod]
        public void TestEvaluateIfStatement()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            var result = scriptApp.Evaluate("if(\"hi\" == \"hi\") { \"hi!\"; }");
            Assert.AreEqual("hi!", result);
        }

        [TestMethod]
        public void TestEvaluateElseStatement()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            var result = scriptApp.Evaluate("if(\"hi\" == \"no\") { \"hi!\"; } else { \"not hi!\"; }");
            Assert.AreEqual("not hi!", result);
        }

        [TestMethod]
        public void TestEvaluateElseIfStatement()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            var result = scriptApp.Evaluate("if(\"hi\" == \"no\") { \"hi!\"; } else { if (\"go\" != \"no\") { \"not go!\"; } }");
            Assert.AreEqual("not go!", result);
        }

        [TestMethod]
        public void TestEvaluateComplex()
        {
            var grammar = new SimpleStringGrammar();
            var scriptApp = new ScriptApp(new LanguageData(grammar));
            scriptApp.Globals["parameters"] = new Dictionary<string, string> { { "OwnerLastName", "Isaak" } };
            var result = scriptApp.Evaluate("if(concat(\"Isa\", \"ak\") == ${OwnerLastName}) { \"hi Scott!\"; }");
            Assert.AreEqual("hi Scott!", result);
        }
    }
}
