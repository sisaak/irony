﻿using System;
using Avala.Irony;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Irony.Tests
{
    [TestClass]
    public class SimpleStringGrammarTests
    {
        [TestMethod]
        public void TestConstructor()
        {
            var grammar = new SimpleStringGrammar();
        }

        [TestMethod]
        public void TestParseLiteral()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("\"hi\";");
            Assert.IsFalse(program.HasErrors());
        }

        [TestMethod]
        public void TestParseVariable()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("${OwnerLastName};");
            Assert.IsFalse(program.HasErrors());
        }

        [TestMethod]
        public void TestParseConcatCall()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("concat(\"hi\", \" ok\");");
            Assert.IsFalse(program.HasErrors());
        }

        [TestMethod]
        public void TestParseReplaceCall()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("replace(\"hi you!\", \"you\", \"Scott\");");
            Assert.IsFalse(program.HasErrors());
        }

        [TestMethod]
        public void TestParseIfStatement()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("if(\"hi\" == \"hi\") { \"hi!\"; }");
            Assert.IsFalse(program.HasErrors());
        }

        [TestMethod]
        public void TestParseElseStatement()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("if(\"hi\" == \"hi\") { \"hi!\"; } else { \"not hi!\"; }");
            Assert.IsFalse(program.HasErrors());
        }

        [TestMethod]
        public void TestParseElseIfStatement()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("if(\"hi\" == \"hi\") { \"hi!\"; } else { if (\"hi\" != \"hi\") { \"not hi!\"; } }");
            Assert.IsFalse(program.HasErrors());
        }

        [TestMethod]
        public void TestParseComplex()
        {
            var grammar = new SimpleStringGrammar();
            var parser = new Parser(grammar);
            var program = parser.Parse("if(concat(\"Isa\", \"ak\") == ${OwnerLastName}) { \"hi Scott!\"; }");
            Assert.IsFalse(program.HasErrors());
        }
    }
}
