﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avala.Irony.Nodes;
using Irony.Interpreter;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace Avala.Irony
{
    [Language("SimpleString", "0.1", "A simple language for string business logic.")]
    public class SimpleStringGrammar : InterpretedLanguageGrammar
    {
        public SimpleStringGrammar()
            : base(true)
        {

            //non terminals
            var stringLiteral = TerminalFactory.CreateCSharpString("StringLiteral");
            var identifier = TerminalFactory.CreateCSharpIdentifier("Identifier");

            //symbols
            var semicolon = ToTerm(";", "semicolon");
            var optionalSemicolon = new NonTerminal("optionalSemicolon");
            optionalSemicolon.Rule = Empty | semicolon;
            var leftBrace = ToTerm("{", "leftBrace");
            var dollarLeftBrace = ToTerm("${", "dollarLeftBrace");
            var rightBrace = ToTerm("}", "rightBrace");
            var leftParenthesis = ToTerm("(", "leftParenthesis");
            var rightParenthesis = ToTerm(")", "rightParenthesis");
            var comma = ToTerm(",", "comma");

            //non-terminals
            //var program = new NonTerminal("program");
            //var statementList = new NonTerminal("statementList");
            var expression = new NonTerminal("expression", typeof(EvaluateFirstChildNode));
            var statement = new NonTerminal("statement", typeof(EvaluateFirstChildNode));
            var embeddedStatement = new NonTerminal("embeddedStatement", typeof(EvaluateFirstChildNode));
            var ifStatement = new NonTerminal("ifStatement", typeof(IfStatementNode));
            var elseClauseOptional = new NonTerminal("elseClauseOptional", typeof(ElseClauseOptionalNode));
            var argumentList = new NonTerminal("argumentList", typeof(ArgumentListNode));
            var functionCall = new NonTerminal("functionCall", typeof(Avala.Irony.Nodes.FunctionCallNode));
            var variable = new NonTerminal("variable", typeof(VariableNode));
            var literal = new NonTerminal("literal", typeof(LiteralNode));
            var binaryOperation = new NonTerminal("binaryOperation", typeof(AstNode));
            var binaryOperationExpression = new NonTerminal("binaryOperationExpression", typeof(BinaryOperationExpressionNode));

            //operators and punctuation
            RegisterOperators(1, "||");
            RegisterOperators(2, "&&");
            RegisterOperators(3, "==", "!=");
            RegisterOperators(4, "+");

            MarkPunctuation(";", "{", "}", "(", ")", ",");
            MarkTransient(binaryOperation);

            AddTermsReportGroup("constant", stringLiteral);
            AddTermsReportGroup("statement", "if");
            AddTermsReportGroup("constant", "true", "false", "null");
            AddTermsReportGroup("unary operator", "+");

            AddToNoReportGroup(comma, semicolon);
            AddToNoReportGroup(leftBrace, dollarLeftBrace, rightBrace);

            //setup rules
            statement.Rule = ifStatement | expression + semicolon;
            embeddedStatement.Rule = leftBrace + statement + rightBrace;
            ifStatement.Rule = ToTerm("if") + leftParenthesis + expression + rightParenthesis + embeddedStatement + elseClauseOptional;
            elseClauseOptional.Rule = Empty | PreferShiftHere() + "else" + embeddedStatement;
            argumentList.Rule = MakePlusRule(argumentList, comma, expression);
            functionCall.Rule = identifier + leftParenthesis + argumentList + rightParenthesis;
            variable.Rule = dollarLeftBrace + identifier + rightBrace;
            literal.Rule = stringLiteral | "true" | "false" | "null";
            expression.Rule = variable | functionCall | literal | binaryOperationExpression;
            binaryOperation.Rule = ToTerm("==") | "!=";
            binaryOperationExpression.Rule = expression + binaryOperation + expression;

            this.Root = statement;
        }
    }
}
