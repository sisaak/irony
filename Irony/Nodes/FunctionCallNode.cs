﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;

namespace Avala.Irony.Nodes
{
    public class FunctionCallNode : AstNode
    {
        public string Name { get; set; }
        public ArgumentListNode ArgumentList { get; set; }
        
        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Name = treeNode.ChildNodes[0].FindTokenAndGetText();
            ArgumentList = treeNode.ChildNodes[1].AstNode as ArgumentListNode;
        }

        protected override object DoEvaluate(global::Irony.Interpreter.ScriptThread thread)
        {
            thread.CurrentNode = this;
            try
            {
                switch (Name)
                {
                    case "concat":
                        return EvaluateConcat(thread);
                    case "replace":
                        return EvaluateReplace(thread);
                    default:
                        throw new InvalidOperationException(string.Format("Function {0} not supported.", Name));
                }
            }
            finally
            {
                thread.CurrentNode = Parent;
            }
        }

        public object EvaluateConcat(global::Irony.Interpreter.ScriptThread thread)
        {
            string val = null;
            foreach(var argument in ArgumentList.Arguments)
            {
                var argumentValue = argument.Evaluate(thread) as string;
                if(argumentValue != null)
                    val += argumentValue;
            }
            return val;
        }

        public object EvaluateReplace(global::Irony.Interpreter.ScriptThread thread)
        {
            if (ArgumentList.Arguments.Count != 3)
                throw new InvalidOperationException(string.Format("The replace function expects 3 arguments: [0]orignal, [1]search, [2]replacewith"));

            string val = null;
            var originalNode = ArgumentList.Arguments.ElementAtOrDefault(0);
            var searchNode = ArgumentList.Arguments.ElementAtOrDefault(1);
            var replaceWithNode = ArgumentList.Arguments.ElementAtOrDefault(2);

            var original = originalNode.Evaluate(thread) as string;
            if (original == null)
                return null;
            var search = searchNode.Evaluate(thread) as string;
            var replaceWith = replaceWithNode.Evaluate(thread) as string;

            return original.Replace(search, replaceWith);
        }

        
    }
}
