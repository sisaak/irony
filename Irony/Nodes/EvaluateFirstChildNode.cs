﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;

namespace Avala.Irony.Nodes
{
    public class EvaluateFirstChildNode : AstNode
    {
        AstNode FirstChild { get; set; }

        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            FirstChild = treeNode.ChildNodes[0].AstNode as AstNode;

            if (FirstChild == null)
                throw new InvalidOperationException("Could not find first child.");
        }

        protected override object DoEvaluate(global::Irony.Interpreter.ScriptThread thread)
        {
            thread.CurrentNode = this;
            try
            {
                var childValue = FirstChild.Evaluate(thread);
                return childValue;
            }
            finally
            {
                thread.CurrentNode = Parent;
            }
        }
    }
}
