﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;

namespace Avala.Irony.Nodes
{
    public class ArgumentListNode : AstNode
    {
        public List<AstNode> Arguments { get; set; }
        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Arguments = new List<AstNode>();
            foreach (var childNode in treeNode.ChildNodes)
            {
                Arguments.Add(childNode.AstNode as AstNode);
            }
        }
    }
}
