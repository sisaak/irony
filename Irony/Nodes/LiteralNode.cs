﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace Avala.Irony.Nodes
{
    public class LiteralNode : AstNode
    {
        public Token Token { get; set; }

        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Token = treeNode.FindToken();
        }

        protected override object DoEvaluate(global::Irony.Interpreter.ScriptThread thread)
        {
            thread.CurrentNode = this;
            try
            {
                if (Token.Value == "true")
                    return true;
                else if (Token.Value == "false")
                    return false;
                else if (Token.Value == "null")
                    return null;
                else
                    return Token.ValueString;
            }
            finally
            {
                thread.CurrentNode = Parent;
            }
        }
    }
}
