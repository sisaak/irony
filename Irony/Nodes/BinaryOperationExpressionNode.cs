﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;

namespace Avala.Irony.Nodes
{
    public class BinaryOperationExpressionNode : AstNode
    {
        public AstNode Left { get; set; }
        public string Operation { get; set; }
        public AstNode Right { get; set; }

        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Left = treeNode.ChildNodes[0].AstNode as AstNode;
            Operation = treeNode.ChildNodes[1].FindTokenAndGetText();
            Right = treeNode.ChildNodes[2].AstNode as AstNode;

            if (Left == null)
                throw new InvalidOperationException("Could not find left expression.");
            if (Right == null)
                throw new InvalidOperationException("Could not find right expression.");
        }

        protected override object DoEvaluate(global::Irony.Interpreter.ScriptThread thread)
        {
            thread.CurrentNode = this;
            try
            {
                var leftValue = Left.Evaluate(thread);
                if (leftValue == null)
                    return false;
                var rightValue = Right.Evaluate(thread);
                if (rightValue == null)
                    return false;

                bool operationValue;
                switch (Operation)
                {
                    case "==":
                        operationValue = Equals(leftValue, rightValue);
                        break;
                    case "!=":
                        operationValue = !Equals(leftValue, rightValue);
                        break;
                    default:
                        throw new InvalidOperationException(string.Format("Operation {0} not imlemented.", Operation));
                }
                return operationValue;
            }
            finally
            {
                thread.CurrentNode = Parent;
            }
        }

        private bool Equals(object left, object right)
        {
            if (left is string && right is string)
                return string.Equals((string)left, (string)right, StringComparison.CurrentCulture);
            else
                return left == right;
        }
    }
}
