﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;

namespace Avala.Irony.Nodes
{
    public class ElseClauseOptionalNode : AstNode
    {
        public AstNode Block { get; set; }

        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            if(treeNode.ChildNodes.Any())
                Block = treeNode.ChildNodes[1].AstNode as AstNode;
        }

        protected override object DoEvaluate(global::Irony.Interpreter.ScriptThread thread)
        {
            thread.CurrentNode = this;
            try
            {
                if (Block == null)
                    return null;
                return Block.Evaluate(thread);
            }
            finally
            {
                thread.CurrentNode = Parent;
            }
        }
    }
}
