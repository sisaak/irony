﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;

namespace Avala.Irony.Nodes
{
    public class IfStatementNode : AstNode
    {
        public AstNode IfExpression { get; set; }
        public AstNode InnerBlock { get; set; }
        public AstNode ElseExpression { get; set; }

        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            IfExpression = treeNode.ChildNodes[1].AstNode as AstNode;
            InnerBlock = treeNode.ChildNodes[2].AstNode as AstNode;
            ElseExpression = treeNode.ChildNodes[3].AstNode as AstNode;

            if (IfExpression == null)
                throw new InvalidOperationException("Could not find if expression.");
            if (InnerBlock == null)
                throw new InvalidOperationException("Could not find inner block.");
            if (ElseExpression == null)
                throw new InvalidOperationException("Could not find else expression.");
        }

        protected override object DoEvaluate(global::Irony.Interpreter.ScriptThread thread)
        {
            thread.CurrentNode = this;
            try
            {
                var expVal = IfExpression.Evaluate(thread);
                var ifExpressionValue = (bool)expVal;
                if (ifExpressionValue)
                    return InnerBlock.Evaluate(thread);
                else
                    return ElseExpression.Evaluate(thread);
            }
            finally
            {
                thread.CurrentNode = Parent;
            }
        }
    }
}
