﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Interpreter.Ast;

namespace Avala.Irony.Nodes
{
    public class VariableNode : AstNode
    {
        public string Name { get; set; }

        public override void Init(global::Irony.Ast.AstContext context, global::Irony.Parsing.ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            Name = treeNode.ChildNodes[1].FindToken().ValueString;
        }

        protected override object DoEvaluate(global::Irony.Interpreter.ScriptThread thread)
        {
            thread.CurrentNode = this;
            try
            {
                var parameters = thread.App.Globals["parameters"] as Dictionary<string, string>;
                if (parameters != null && parameters.ContainsKey(Name))
                {
                    return parameters[Name];
                }
                return null;
            }
            finally
            {
                thread.CurrentNode = Parent;
            }
        }
    }
}
